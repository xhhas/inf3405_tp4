// Chatroom-Client.cpp : Defines the entry point for the console application.

/*
Client Main
Author: Xhulio Hasani
Author : Antoine Valantin
Date : 12 novembre 2017
*/

#include "stdafx.h"


#define WIN32_LEAN_AND_MEAN

//#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <thread>



// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


#define DEFAULT_BUFLEN 226 //we define the message length to be a maximim of 200 characters
#define DEFAULT_PORT "5050" //for debug
bool allover = false;
int ReceptionThreadValue = 0;

//a struct to keep data info: equivalent of client.cpp on server side
struct ClientData
{
	SOCKET socket;
	int ID;
	std::string username;
	char receivedMessage[DEFAULT_BUFLEN];
};

//method that  recieves the other clients messages : on separate thread
int clientReciever(ClientData &client)
{
	while (!allover)
	{
		memset(client.receivedMessage, 0, DEFAULT_BUFLEN);
		if (client.socket != 0)
		{
			int iResult = recv(client.socket, client.receivedMessage, DEFAULT_BUFLEN+1, 0);
			if (iResult != SOCKET_ERROR)
				std::cout << client.receivedMessage << std::endl;
			else
			{
				allover = true;
				break;
			}
		}
	}
	if (WSAGetLastError() == WSAECONNRESET)
	{
		std::cout << "The server has disconnected" << std::endl;
		allover = true;
		ReceptionThreadValue = 1;
		return 1;
	}
		
	return 0;
}


int main()
{
	int CheckPort = 0;
	std::string serverAddr = "127.0.0.1";
	std::string serverPort = "5050";
	

	//Validating the port address
	while (CheckPort != 1) {
		CheckPort = 0;
		std::cout << "Type the port of the server between 5000 and 5050 : " << std::endl;
		getline(std::cin, serverPort);
		int intPort = 0;
		try//if entered port is not a valid int, throws invalid argument excp.
		{
			intPort = std::stoi(serverPort);
		}
		catch (const std::invalid_argument& ia)//catching exception
		{
			CheckPort = 2;
		}
		//checking if port adress is in our bounderies
		if (CheckPort != 2 && intPort >= 5000 && intPort <= 5050 && serverPort.size() == 4) {
			CheckPort = 1;
		}
		else {
			std::cout << "Error ! Please make sure the port of the server is well typed" << std::endl;
		}
	}

	//Validating the ip address
	int addrOk = 0;
	while (addrOk != 1) {
		std::cout << "" << std::endl;
		std::cout << "Type the address of the server : " << std::endl;
		std::cout << "(Local host is 127.0.0.1)" << std::endl;
		getline(std::cin, serverAddr);
		struct sockaddr_in sa;
		addrOk = inet_pton(AF_INET, serverAddr.c_str(), &(sa.sin_addr));
		if(addrOk != 1)
			std::cout << "Error ! Please make sure the address of the server is well typed and on 8 bytes" << std::endl;
	}

	WSADATA wsaData;
	SOCKET ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;
	char *sendbuf = "this is a test";
	char recvbuf[DEFAULT_BUFLEN];
	int iResult;
	int recvbuflen = DEFAULT_BUFLEN;


	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	iResult = getaddrinfo(serverAddr.c_str(), serverPort.c_str(), &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return 1;
		}

		// Connect to server.
		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);
	//if no response from server :
	if (ConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server!\n");
		WSACleanup();
		return 1;
	}


	std::string message;
	ClientData client;
	client.socket = ConnectSocket;
	client.ID = -1; //TODO
	std::string connexionInput = "";

	while (1) {
		//Login menu
		std::cout << "" << std::endl;
		std::cout << "Log In menu" << std::endl;
		std::cout << "" << std::endl;
		std::cout << "If you are a new user, enter : 1" << std::endl;
		std::cout << "If you already have an account, enter : 2" << std::endl;
		std::cout << "" << std::endl;
		std::cin >> connexionInput;

		if (connexionInput == "1") //if new user
		{
			std::cout << "Enter your new username : " << std::endl;
			std::cin >> client.username;
			std::cout << "Enter your new password : " << std::endl;
			std::string password;
			std::cin >> password;
			std::string msg = "1:"+client.username + ":" + password;
			iResult = send(ConnectSocket, msg.c_str(), strlen(msg.c_str()), 0);
			break;

		}
		else if (connexionInput == "2") //if existing user
		{
			std::cout << "Enter your username : " << std::endl;
			std::cin >> client.username;
			std::cout << "Enter your password : " << std::endl;
			std::string password;
			std::cin >> password;
			std::string msg = "2:" + client.username + ":" + password;
			//sending request to server
			iResult = send(ConnectSocket, msg.c_str(), strlen(msg.c_str()), 0);
			//recieving answer
			int iResult = recv(ConnectSocket, recvbuf, DEFAULT_BUFLEN, 0);
			if (iResult != SOCKET_ERROR)
			{
				std::string recvstr(recvbuf);
				recvstr = recvstr.substr(0, 8);
				if (recvstr == "ACCEPTED")//Username and password were correct
				{
					std::cout << "Server response: " +recvstr << std::endl;
					break;
				}
				else //bad username or password
				{
					std::cout << "Access denied : Username does not exist or password does not match." << std::endl;
				}
			}
			else
			{
				std::cout << "ERROR : problem in reception" << std::endl;
			}

		}
		else
		{
			std::cout << "Not a valid option" << std::endl;
			std::cout << "" << std::endl;
		}
	}


	// Receive until the peer closes the connection
	std::cout << "" << std::endl;
	std::cout << "WELCOME TO THE CHATROOM" << std::endl;
	std::cout << "Ready to send messages" << std::endl;
	std::cout << "Enter \"quit\" to leave the chatroom" << std::endl;
	std::thread receptionThread(clientReciever, client);
	do {

		getline(std::cin, message);

		if (message != "")
		{	
			if (message == "quit" || (ReceptionThreadValue == 1))//If we want to quit
			{
				std::cout << "Exiting chatroom" << std::endl;
				std::string quit = "";
				iResult = send(ConnectSocket, quit.c_str(), strlen(quit.c_str()), 0);
				iResult = shutdown(ConnectSocket, SD_SEND);
				receptionThread.detach();
				if (iResult == SOCKET_ERROR) {
					printf("shutdown failed: %d\n", WSAGetLastError());
					closesocket(ConnectSocket);
					WSACleanup();
					return 1;
				}
				break;
			}
			else
			{
				iResult = send(ConnectSocket, message.c_str(), strlen(message.c_str())+1, 0);
			}

			if (iResult == SOCKET_ERROR) {
				printf("send failed with error: %d\n", WSAGetLastError());
				closesocket(ConnectSocket);
				WSACleanup();
				return 1;
			}
		}

	} while ((iResult > 0 )&& (message != "quit")&&!allover);
	std::cout << "Transmission over " << std::endl;

	// shutdown the connection since no more data will be sent
	iResult = shutdown(ConnectSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		printf("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 1;
	}

	// cleanup
	closesocket(ConnectSocket);
	WSACleanup();
	
	return 0;
}

