/*
	Server header:
	Author: Xhulio Hasani
	Author: Antoine Valantin
	Date : 12 novembre 2017
*/
#pragma once
#include <stack>
#include <utility>
#include <string>
#include <map>
#include <thread>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <vector>
#include "Client.h"
#include <list>


class Server
{
public:
	//ctor
	Server();
	//dtor
	~Server();
	//the methode that takes care of individual clients communication: on separate thread
	int processClient(Client & client);
	//method that will listen the commands entered on the server's console : on separate thread
	std::string commandListener();
	//method that intilizes the server by reading the files on disk and loading data into data structure
	void init();
	//main methode :  Winsock setup and clients log in: on main thread
	int run();
	//method that writes on files to save data on disk
	void close();
	//vector with that keeps the clients currently connected to the server
	std::vector<Client> currentClients;
	//number of clients currently connected
	int numberClients ;
	//boolean for propagation of closing state for the other threads
	bool isClosing;


private:
	//data structre that keeps track of message memory
	std::list<std::string> messagesInMemory;
	//data structure that keeps the different users account and passwords
	std::map<std::string, std::string> RegisteredClients;
	//command entered on server's console
	std::string command;
	
};

