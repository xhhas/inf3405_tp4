/*
Client.cpp
Author: Xhulio Hasani
Author : Antoine Valantin
Date : 12 novembre 2017
*/

#include "stdafx.h"
#include "Client.h"
#include <winsock2.h>
#include <ws2tcpip.h>


//creator
Client::Client()
{
	ID = 0;
	socket = INVALID_SOCKET;
}

//creator with parameters
Client::Client(int i, SOCKET s)
{
	ID = i;
	socket = s;
}
//destructor
Client::~Client()
{
}
