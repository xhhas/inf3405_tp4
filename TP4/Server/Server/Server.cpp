/*
Classe Server:
Author: Xhulio Hasani
Author : Antoine Valantin
Date : 12 novembre 2017
*/

#include "stdafx.h"
#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <thread>
#include <conio.h>
#include <stack>
#include "Server.h"
#include "Client.h"
#include <algorithm>
#include <time.h>
#include <list>
#include <chrono>
#include <thread>


// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")

#define DEFAULT_BUFLEN 226
#define DEFAULT_PORT "5050"
#define MAX_CLIENTS 5

//booleans for status of separate threads
bool commandThreadStatus = false;
bool clientProcessingThreadStatus = false;

//default creator
Server::Server()
{
	std::vector<Client> currentClients(MAX_CLIENTS);  //ne semble pas faire ce qui est attendu
	int numberClients = 0;

}

//destructor
Server::~Server()
{
}

//processing the client
int Server::processClient(Client &client)
{
	clientProcessingThreadStatus = true;
	std::string message = "";
	char tempmessage[DEFAULT_BUFLEN] = "";
	//non-blocking mode for the socket
	u_long mode = 1;
	ioctlsocket(client.socket, FIONBIO, &mode);
	int iResult = 1;
	time_t now = time(0);

	while (!isClosing)
	{
		if (client.socket != 0)
		{	
				//reseting the recievebuffer 
				memset(tempmessage, 0, DEFAULT_BUFLEN);
				//recieving data
				iResult = recv(client.socket, tempmessage, DEFAULT_BUFLEN, 0);
		
			if (isClosing) //if server closing, do not continue
			{
				clientProcessingThreadStatus = false;
				return 0;
			}
				
			//if we recieved a message
			if (iResult != SOCKET_ERROR && iResult != 0)
			{
				//to have time stamp before message
				time_t result = time(NULL);
				char str[26];
				if (strcmp("", tempmessage)) {
					ctime_s(str, sizeof str, &result);
					str[strlen(str) - 1] = '\0';
					std::string strng(str);
					message = strng +" : " + client.username + " : " + tempmessage;
				}
				std::cout << message.c_str() << std::endl;
				messagesInMemory.push_back(message);

				//Broadcast that message to the other clients
				for (int i = 0; i < currentClients.size(); i++)
				{
					//std::cout << "sent to" + std::to_string(currentClients[i].ID) << std::endl;
					if (currentClients[i].socket != INVALID_SOCKET)
						if (client.ID != i)
							iResult = send(currentClients[i].socket, message.c_str(), strlen(message.c_str())+1, 0);
				}

			}
			//if we loose the connection
			if(iResult == 0)
			{
				message = (client.username) + " Disconnected";
				std::cout << message << std::endl;
				//Broadcast the disconnection message to the other clients
				for (int i = 0; i < currentClients.size(); i++)
				{
					if (currentClients[i].socket != INVALID_SOCKET && currentClients[i].ID != client.ID)
						iResult = send(currentClients[i].socket, message.c_str(), strlen(message.c_str())+1, 0);
				}

				closesocket(currentClients[client.ID].socket);
				numberClients--;
				client.socket = INVALID_SOCKET;
				client.ID = -1;
				client.username = "";
				//currentClients.erase(std::remove(currentClients.begin(), currentClients.end(), client), currentClients.end());
				clientProcessingThreadStatus = false;
				return 0;
			}
		}
	}
	clientProcessingThreadStatus = false;//thread over
	return 0;
}


//method that keeps track of the server's comand
std::string Server::commandListener()
{
	commandThreadStatus = true;
	while (!isClosing) {
		getline(std::cin, command);
		if (command == "close")
			isClosing = true;;
	}
	commandThreadStatus = false;
	return command;
}


//intialization of server: reading files into memory
void Server::init()
{
	/*
	Read client database and put in hashmap
	*/
	std::ifstream clientsdatafile;
	clientsdatafile.open("ClientsData.txt");
	std::string line;
	std::string name;
	std::string password;
	if (clientsdatafile.is_open())
	{
		while (getline(clientsdatafile, line))
		{
			name = line.substr(0, line.find(" "));
			password = line.substr(line.find(" ") + 1, line.size() - 1);
			RegisteredClients.insert(std::pair<std::string, std::string>(name, password));
		}
		clientsdatafile.close();
	}

	


	/*
	Loading message history into memory
	*/
	std::ifstream msghistory;
	msghistory.open("MessageHistory.txt");
	if (msghistory.is_open()) {
		while (std::getline(msghistory, line))
		{
			messagesInMemory.push_back(line);
		}
	}
	msghistory.close();
}

//main thread: winsock set up and client login
int Server::run()
{	
	//we will stock the created threads processing each clients in a vector
	std::vector<std::thread> threads;
	//intialising a message
	std::string message = "";
	WSADATA wsaData;
	int iResult;

	int CheckPort = 0;
	std::string serverAddr = "127.0.0.1";
	std::string serverPort = "5050";
	 
	//Validating the port address
	while (CheckPort != 1) {
		CheckPort = 0;
		std::cout << "Type the port of the server between 5000 and 5050 : " << std::endl;
		getline(std::cin, serverPort);
		int intPort = 0;
		try//if entered port is not a valid int, throws invalid argument excp.
		{
			intPort = std::stoi(serverPort);
		}
		catch (const std::invalid_argument& ia)//catching exception
		{
			CheckPort = 2;
		}		
		//checking if port adress is in our bounderies
		if (CheckPort != 2 && intPort >= 5000 && intPort <= 5050 && serverPort.size() == 4) {
			CheckPort = 1;
		}
		else {
			std::cout << "Error ! Please make sure the port of the server is well typed" << std::endl;
		}
	}

	//Validating the ip address
	int addrOk = 0;
	while (addrOk != 1) {
		std::cout << "" << std::endl;
		std::cout << "Type the address of the server : " << std::endl;
		std::cout << "(Local host is 127.0.0.1)" << std::endl;
		getline(std::cin, serverAddr);
		struct sockaddr_in sa;
		addrOk = inet_pton(AF_INET, serverAddr.c_str(), &(sa.sin_addr));
		if (addrOk != 1)
			std::cout << "Error ! Please make sure the address of the server is well typed and on 8 bytes" << std::endl;
	}

	//initializing the sockets
	SOCKET ListenSocket = INVALID_SOCKET;
	SOCKET ClientSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL;
	struct addrinfo hints;
	int iSendResult;
	char recvbuf[DEFAULT_BUFLEN] = "";
	int recvbuflen = DEFAULT_BUFLEN;

	// Initialize Winsock
	std::cout << "Initializing Winsock" << std::endl;
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	std::cout << "Resolving the server address and port" << std::endl;
	iResult = getaddrinfo(serverAddr.c_str(), serverPort.c_str(), &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Create a SOCKET for listening
	std::cout << "Creating a socket for listening" << std::endl;
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	// Setup the TCP listening socket
	std::cout << "Setting up TCP listening socket" << std::endl;
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	freeaddrinfo(result);
	iResult = listen(ListenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}


	//Initialize the client list
	for (int i = 0; i < MAX_CLIENTS; i++)
	{
		Client* n = new Client(-1, INVALID_SOCKET);
		currentClients.push_back(*n);
	}

	//creating a thread for the commandListener
	std::thread commandThread(&Server::commandListener, this);
	u_long iMode = 1;

	std::cout << "" << std::endl;
	std::cout << "ENTER \"close\" TO CLOSE THE SERVER AND SAVE ALL DATA." << std::endl;
	std::cout << "" << std::endl;
	do {//login of new connexion

		//Enabling non blocking mode for the ListenSocket
		ioctlsocket(ListenSocket, FIONBIO, &iMode);
		// Accept a client socket
		ClientSocket = accept(ListenSocket, NULL, NULL);
	AcceptingClients:	//for goto

		int tempToken = -1;
		if (ClientSocket != INVALID_SOCKET) {

			std::cout << "Connection accepted : processing login" << std::endl;
			for (int i = 0; i < currentClients.size(); i++)
			{
				if (currentClients[i].socket == INVALID_SOCKET && tempToken == -1)
				{
					currentClients[i].socket = ClientSocket;
					currentClients[i].ID = i;
					tempToken = i;
					numberClients++;
				}
			}
			if (tempToken != -1)
			{
				std::string recstr;
				memset(recvbuf, 0, DEFAULT_BUFLEN);
				//Treating the login of the client
				//disabling non blocking md
				u_long zero = 0;
				ioctlsocket(currentClients[tempToken].socket, FIONBIO, &zero);
				int iResult = recv(currentClients[tempToken].socket, recvbuf, DEFAULT_BUFLEN, 0);
				if (recvbuf[0] == '1')
				{
					//new user
					std::string recvstr(recvbuf);
					std::cout << recvstr << std::endl;
					//add the new user in database
					currentClients[tempToken].username = recvstr.substr(2, recvstr.find(":", 2) - 2);
					std::string password = recvstr.substr(recvstr.find(":", 3) + 1, std::string::npos);
					//Registering the new user
					RegisteredClients.insert(std::pair<std::string, std::string>(currentClients[tempToken].username, password));

				}
				else if (recvbuf[0] == '2')
				{
					//existing user
					std::string recvstr(recvbuf);
					std::string attemptedusername = recvstr.substr(2, recvstr.find(":", 2) - 2);
					std::string password = recvstr.substr(recvstr.find(":", 3) + 1, std::string::npos);
					//Check if they are the same

					if (RegisteredClients.count(attemptedusername) != 0) {
						if (RegisteredClients.find(attemptedusername)->second == password)
						{
							//it means the user was found and password matches
							message = "ACCEPTED\0";
							send(currentClients[tempToken].socket, message.c_str(), strlen(message.c_str()), 0);
							currentClients[tempToken].username = attemptedusername;
							//may continue

						}
						else
						{
							// password does not match
							message = "ACCESS DENIED\0";
							std::cout << message + " : log in failed."<< std::endl;
							send(currentClients[tempToken].socket, message.c_str(), strlen(message.c_str()), 0);
							currentClients[tempToken].socket = INVALID_SOCKET;
							tempToken--;
							goto AcceptingClients;
						}
					}
					else {
						//user was not found
						message = "ACCESS DENIED\0";
						std::cout << message + " : log in failed." << std::endl;
						send(currentClients[tempToken].socket, message.c_str(), strlen(message.c_str()), 0);
						currentClients[tempToken].socket = INVALID_SOCKET;
						tempToken--;
						goto AcceptingClients;

					}

				}
				//Send the id to that client
				message = std::to_string(currentClients[tempToken].ID);
				std::cout << "New client  : " << currentClients[tempToken].username << " Accepted with ID : " << message << std::endl;	
				send(currentClients[tempToken].socket, message.c_str(), strlen(message.c_str())+1, 0);

				//Sending last 15 messages in history
				message = "Here are the last messages:";
				std::this_thread::sleep_for(std::chrono::milliseconds(1));
				send(currentClients[tempToken].socket, message.c_str(), strlen(message.c_str())+1, 0);
				std::list<std::string>::iterator it = messagesInMemory.end();
				std::string lastmessage;
				int i = (std::min)(15, (int)messagesInMemory.size());
				for (int j = 0; j < i; j++) { it--; }
				
				for (; it != messagesInMemory.end(); ++it)
				{	
					memset(&lastmessage, '\0', DEFAULT_BUFLEN);	
					lastmessage = *it;
					std::this_thread::sleep_for(std::chrono::milliseconds(1));
					send(currentClients[tempToken].socket, lastmessage.c_str(), strlen(lastmessage.c_str()) + 1, 0);
				}
				

				//Creating a thread for the client
				threads.push_back( std::thread(&Server::processClient, this, std::ref(currentClients[tempToken])));

			}
			else
			{
				message = "Server is full";
				send(ClientSocket, message.c_str(), strlen(message.c_str()), 0);
				std::cout << message << std::endl;
			}
		}

	} while (!isClosing);

	// No longer need listening socket
	closesocket(ListenSocket);
	
	// shutdown the connection since we're done
	iResult = shutdown(ClientSocket, SD_SEND);
	// cleanup
	closesocket(ClientSocket);
	WSACleanup();

	while (commandThreadStatus||clientProcessingThreadStatus)
	{
		//we have to wait for threads to end; will generally not be needed
	}
	commandThread.join();
	for (int i = 0; i < threads.size(); i++)
	{
		threads[i].join();//joining all threads to secure close up
		closesocket(currentClients[i].socket);//closing all sockets
	}
	currentClients.clear();//clearing vector
	return 0;
}

void Server::close()
{
	/*
	*	Writing clients data into file
	*/
	std::ofstream clientsdatafile("ClientsData.txt");
	if (clientsdatafile.is_open())
	{
		for (std::map<std::string, std::string>::iterator it = RegisteredClients.begin(); it != RegisteredClients.end(); ++it)
		{
			clientsdatafile << it->first << " " << it->second << '\n';
		}
	}

	/*
	*	Writing message memory into file
	*/
	std::ofstream msgMemory("MessageHistory.txt");
	if (msgMemory.is_open())
	{
		int s = messagesInMemory.size();
		for (int i = 0; i < s; i++)
		{
			msgMemory << messagesInMemory.front() << '\n';
			messagesInMemory.pop_front();
		}
	}

	std::cout << " All data saved succesfully" << std::endl;
}
