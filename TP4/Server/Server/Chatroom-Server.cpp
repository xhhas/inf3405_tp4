// Chatroom-Server.cpp : Defines the entry point for the console application.

/*
Server Main
Author: Xhulio Hasani
Author : Antoine Valantin
Date : 12 novembre 2017
*/


#include "stdafx.h"
#include "Server.h"

using namespace std;

int main()
{
	//we start by creating a server
	Server server;
	//intializing the server
	server.init();
	//running main activity
	server.run();
	//closing correctly the server: saving all data on disk
	server.close();
	system("pause");//to keep window opoen
    return 0;
}

