/*
Client header
Author: Xhulio Hasani
Author : Antoine Valantin
Date : 12 novembre 2017
*/

#pragma once
#include "stdafx.h"
#include <winsock2.h>
#include <ws2tcpip.h>
#include <string>

class Client
{
public:
	//ctor
	Client();
	//parameter ctor
	Client(int i, SOCKET s);
	//dtor
	~Client();
	//we keep the attributes public for simplicity; they should ideally be private with accessors
	int ID;
	SOCKET socket;
	std::string username;

	//operator overload
	friend bool operator == (Client client1, Client client2)
	{
		if (client1.ID == client2.ID) return true;
		else return false;
	}

};